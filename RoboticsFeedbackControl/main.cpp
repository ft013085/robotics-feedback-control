#include <iostream>

class FeedbackControlSystem {
private:
    double desired; // Desired input
    double actual;  // Actual output
    double error;   // Error term
    double deviceOut; // Output of the device
    double controllerGain; // Controller gain
    double deviceGain; // Device gain

public:
    FeedbackControlSystem(double desiredValue, double controller, double device)
        : desired(desiredValue), actual(0), error(0), deviceOut(0), controllerGain(controller), deviceGain(device) {}

    void update(double disturbance) {
        // Calculate error
        error = desired - actual;

        // Calculate device output
        deviceOut = error * controllerGain * deviceGain;

        // Simulate disturbance effect
        actual = deviceOut - disturbance;
    }

    double getActual() const {
        return actual;
    }
};

int main() {
    // Constants for the system
    const double desiredVelocity = 1.0; // Desired velocity
    const double controllerGain = 33.0; // Controller gain
    const double deviceGain = 3.0; // Device gain

    // Create a feedback control system with desired velocity and gains
    FeedbackControlSystem system(desiredVelocity, controllerGain, deviceGain);

    // Simulate disturbance
    double disturbance = 0.0;

    // Update the system with disturbance
    system.update(disturbance);

    // Get the actual velocity
    double actualVelocity = system.getActual();

    // Output the result
    std::cout << "Actual Velocity: " << actualVelocity << std::endl;

    return 0;
}
